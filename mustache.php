<?php
require 'Mustache/Autoloader.php';
Mustache_Autoloader::register();

$m = new Mustache_Engine;

$template = file_get_contents("partials/alert.mustache");
$hash = 
	array(
		'alerts' => array(
			array(
				'message' => 'This was created by Mustache',				
				'close' => true,
				'type' => 'success'),
			array(
				'message' => 'This was also created by Mustache',
				'title' => 'This is title',
				'close' => false,
				'type' => 'error')
			)
		);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.2/mustache.min.js"></script>	
	<script src="http://ajax.aspnetcdn.com/ajax/bootstrap/2.3.1/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/bootstrap/2.3.1/css/bootstrap.min.css" />
	

	<script src="js/global.js"></script>
	<script>		
	

	</script>
</head>
<body>
	<script id="alertsTemplate" type="text/mustache-template">
		<?php include('partials/alert.mustache');?>
	</script>

	<div class="container">
	
		<div class="main">
			<!-- using php -->
			<?=$m->render($template, $hash);?>
		</div>
		
		<footer>
			
		</footer>
	</div>
</body>
</html>