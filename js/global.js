	$('document').ready(function(){
			var template = $('#alertsTemplate').html();
			var compiledTemplate = Mustache.compile(template);
			var hash = {
				'alerts': [
					{
						'title': 'From Javascript',
						'message': 'Mustache works!',
						'type': 'info'
					},
					{
						'message': 'Mustache still works',
						'type': 'success',
						'close': 1
					}
				]
			};
			var output = compiledTemplate(hash);
			
			$('.main').append(output);
		});