<?error_reporting(E_ALL);
$mysql_server = "localhost";
$mysql_user = "root";
$mysql_password = "";
$mysql_db = "playground";
$cxn = mysql_connect($mysql_server, $mysql_user, $mysql_password) or die("Unable to connect to MySQL");
$db = mysql_select_db($mysql_db, $cxn) or die("Could not select ".$mysql_db);


//Select all entries from the menu table
$result = mysql_query("SELECT id, label, link, parent FROM menu ORDER BY parent, sort, label");

//Create the multidimensional array to contain a list of items and parents
$menu = array(
	'items' => array(),
	'parents' => array()
);


//Builds the array lists with data from the menu table
while ($items = mysql_fetch_assoc($result)){
	//Creates entry into items array with current menu item id ie. $menu['items'][1]
	$menu['items'][$items['id']] = $items;

	//Creates entry into parents array. Parents array contains a list of all items with children
	$menu['parents'][$items['parent']][] = $items['id'];

}

//run
// 0 is root 
?>
<!doctype html>
<html lang="en">
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>

  	//Navigation Drop Down Functions
  	$('li:has(ul)').hover(
  	  function(){
        $(this).find('ul:first').prev().addClass('current');
  	    $(this).find('ul:first').fadeIn('fast');
  	  },
  	  function(){
        $(this).find('ul:first').prev().removeClass('current');
  	    $(this).find('ul:first').hide();
  	  }
  	);
	</script>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>

body {
  font-family: 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  padding: 20px 50px 150px;
  font-size: 13px;
  text-align: center;
  background: #E3CAA1;
}
 
*{margin: 0; padding: 0;}
nav{margin: 0 auto; background-color: #000; height:25px;}
nav ul{list-style-type:none;}
nav ul li{float: left; position: relative;}
nav ul li:hover{background-color: #999;}
nav ul li a{color: #fff; padding: 0 30px; line-height:25px; font-size:11px; font-family:arial; display: block; text-decoration:none;}
nav ul li a:hover{background-color: #999;}
nav ul li ul li{float: none; position: relative;}
nav ul li ul{position: absolute; top:25px; left:0; display: none; background-color: #000; width:150px;}
nav ul li:hover > ul{display: block;}
nav ul li ul li a{white-space: nowrap; line-height:25px;}
nav ul li ul li ul{position: absolute; top:0; left:145px; display: none; background-color: #000; width:150px;}


</style>
</head>
<body>
	<div class="container">
		<header>
			<a href="" class="logo"></a>
			<nav>
				<?=buildMenu(0,$menu)?>
			</nav>
		</header>
		<div class="main">
			<div class="welcome">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, dolorem, iure odio impedit exercitationem quam consectetur officiis veniam aperiam excepturi perferendis cumque inventore veritatis pariatur ratione ducimus commodi autem voluptatibus.</p>
			</div>
		</div>
		<footer>
			<div class="baseline">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, dolorum dolore dolorem explicabo ipsa harum itaque totam. Commodi, repellendus fugit dolorem perferendis. Cupiditate, delectus alias nostrum inventore molestiae ullam quisquam!</p>
			</div>
		</footer>
	</div>
</body>
</html>


<?
//SQL
/*
  CREATE TABLE menu (
	id int(11) NOT NULL auto_increment,
	label varchar(50) NOT NULL default '',
	link varchar(100) NOT NULL default '#',
	parent int(11) NOT NULL default '0',
	sort int(11) default NULL,
	PRIMARY KEY (id)
  )

INSERT INTO menu (id, label, link, parent, sort) VALUES (1, 'Home', '#home', 0, 0);
INSERT INTO menu (id, label, link, parent, sort) VALUES (2, 'Code', '#code', 0, 1);
INSERT INTO menu (id, label, link, parent, sort) VALUES (3, 'Contact', '#contact', 0, 3);
INSERT INTO menu (id, label, link, parent, sort) VALUES (4, 'PHP', '#php', 2, NULL);
INSERT INTO menu (id, label, link, parent, sort) VALUES (5, 'CSS', '#css', 2, NULL);
INSERT INTO menu (id, label, link, parent, sort) VALUES (6, 'Scripts', '#scripts', 4, 1);
INSERT INTO menu (id, label, link, parent, sort) VALUES (7, 'Help', '#help', 4, 0);
INSERT INTO menu (id, label, link, parent, sort) VALUES (8, 'Archive', '#archive', 6, NULL);
INSERT INTO menu (id, label, link, parent, sort) VALUES (9, 'Snippets', '#snippets', 8, NULL);
 */



//Menu Builder
function buildMenu($parent,$menu){
	$html = "";
	if (isset($menu['parents'][$parent])){
		$html .= "<ul>\n";

		foreach ($menu['parents'][$parent] as $itemid ) {
			if(!isset($menu['parents']{$itemid})){
				$html .= "<li>\n \t<a href='" . $menu['items'][$itemid]['link'] ."'>" . $menu['items'][$itemid]['label'] . "</a>\n</li> \n";
			}
			if (isset($menu['parents'][$itemid])){
				$html .= "\t<li>\n \t<a href='" .$menu['items'][$itemid]['link'] ."'>" . $menu['items'][$itemid]['label'] . "</a> \n";
				$html .= buildMenu($itemid,$menu);
				$html .= "\t\t</li> \n";
			}

		}
		$html .= "\t</ul>\n";
		
	}
	return $html;
}
?>

<!-- <ul>
   <li>
      <a href='#code'>Code</a> 
      <ul>
         <li>
            <a href='#css'>CSS</a>
         </li>
         <li>
            <a href='#php'>PHP</a> 
            <ul>
               <li>
                  <a href='#help'>Help</a>
               </li>
               <li>
                  <a href='#scripts'>Scripts</a> 
                  <ul>
                     <li>
                        <a href='#archive'>Archive</a> 
                        <ul>
                           <li>
                              <a href='#snippets'>Snippets</a>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </li>
            </ul>
         </li>
      </ul>
   </li>
   <li>
      <a href='#contact'>Contact</a>
   </li>
   <li>
      <a href='#home'>Home</a>
   </li>
</ul> -->