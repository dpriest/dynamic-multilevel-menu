<?error_reporting(E_ALL);
$mysql_server = "localhost";
$mysql_user = "root";
$mysql_password = "";
$mysql_db = "playground";
$cxn = mysql_connect($mysql_server, $mysql_user, $mysql_password) or die("Unable to connect to MySQL");
$db = mysql_select_db($mysql_db, $cxn) or die("Could not select ".$mysql_db);


//Select all entries from the menu table
$result = mysql_query("SELECT id, label, link, parent FROM menu ORDER BY parent, sort, label");

//Create the multidimensional array to contain a list of items and parents
$menu = array(
	'items' => array(),
	'parents' => array()
);


//Builds the array lists with data from the menu table
while ($items = mysql_fetch_assoc($result)){
	//Creates entry into items array with current menu item id ie. $menu['items'][1]
	$menu['items'][$items['id']] = $items;

	//Creates entry into parents array. Parents array contains a list of all items with children
	$menu['parents'][$items['parent']][] = $items['id'];

}

//run
// 0 is root 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
  <!-- http://www.1stwebdesigner.com/design/snippets-html5-boilerplate/ -->
    
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
   	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	
	<!-- For faster page loads -->
	<!--[if IE]><![endif]-->
    <!-- Bootstrap -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/normalize/2.1.0/normalize.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/css/bootstrap.min.css" media="screen" /> -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.1/select2.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.min.css"/>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	

	<!-- Place favicon.ico and apple-touch-icon.png in the root of your domain and delete these references -->

		       <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">



<style>
	/* maxvoltar.com/archive/-webkit-font-smoothing */
	html { -webkit-font-smoothing: antialiased; overflow-y: scroll; }
	body {
		padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		font:13px sans-serif; *font-size:small; *font:x-small; line-height:1.22;
	}

	

	/*
	fonts.css from the YUI Library: developer.yahoo.com/yui/
	Please refer to developer.yahoo.com/yui/fonts/ for font sizing percentages
	*/
	table { font-size:inherit; font:100%; }
	select, input, textarea { font:99% sans-serif; }

	/* makes the text wrap when it reaches the walls of its container */
	pre {
		padding: 15px;
		white-space: pre; /* CSS2 */
		white-space: pre-wrap; /* CSS 2.1 */
		white-space: pre-line; /* CSS 3 (and 2.1 as well, actually) */
		word-wrap: break-word; /* IE */
	}
	/* align checkboxes, radios, text inputs with their label */
	input[type="radio"] { vertical-align: text-bottom; }
	input[type="checkbox"] { vertical-align: bottom; *vertical-align: baseline; }
	.ie6 input { vertical-align: text-bottom; }

	/* hand cursor on clickable input elements */
	label, input[type=button], input[type=submit], button { cursor: pointer; }

	/* bicubic resizing for non-native sized IMG:
	code.flickr.com/blog/2008/11/12/on-ui-quality-the-little-things-client-side-image-resizing/ */
	.ie7 img { -ms-interpolation-mode: bicubic; }
	
	@media all and (orientation:portrait) {
	/* Style adjustments for portrait mode goes here */
	}

	@media all and (orientation:landscape) {
	/* Style adjustments for landscape mode goes here */
	}

/* multilevel menu */
.open .dropdown-toggle,.dropdown.open .dropdown-toggle{color: #666;} 
.open .dropdown-menu,.dropdown.open .dropdown-menu{display:none;}

.open > .dropdown-menu,.dropdown.open > .dropdown-menu{display:block;}
/* flyout */
.open .dropdown-menu > .dropdown:hover > .dropdown-menu, .dropdown.open .dropdown-menu > .dropdown:hover > .dropdown-menu{ display:block; margin-left:157px;}

.dropdown-menu > .dropdown > .dropdown-menu { top: 0px; }

.dropdown-menu > .dropdown > .dropdown-toggle:hover { color:#fff; background-color: #0088cc; }

.dropdown-menu > .dropdown > .dropdown-toggle { background: none; }

.dropdown-menu > .dropdown.offset-left > .dropdown-menu { left: -100%; }

.dropdown-menu > .dropdown.offset-right > .dropdown-menu { left: 100%; }

.dropdown-menu > .dropdown.offset-left > .dropdown-menu:before, .dropdown-menu > 

.dropdown.offset-right > .dropdown-menu:before, .dropdown-menu > .dropdown.offset-left > 

.dropdown-menu:after, .dropdown-menu > .dropdown.offset-right > .dropdown-menu:after { content: ''; display: inline-block; border: 0; position: absolute; }

/*hover*/
/*ul.nav li.dropdown:hover ul.dropdown-menu{
    display: block;
}*/
</style>


    <!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
	<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
	<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
	<!--[if IE 9 ]>    <body class="ie9"> <![endif<]-->
	<!--[if (gt IE 9)|!(IE)]><!-->  <!--<![endif]-->
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <![endif]-->




 

<!-- asynchronous google analytics
change the UA-XXXXX-X to be your site's ID -->
<script>
 var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
 (function(d, t) {
  var g = d.createElement(t),
      s = d.getElementsByTagName(t)[0];
  g.async = true;
  g.src = '//www.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g, s);
 })(document, 'script');
</script>

<style>
/*nav{margin: 0 auto; background-color: #000; height:25px;}
nav ul{list-style-type:none;}
nav ul li{float: left; position: relative;}
nav ul li:hover{background-color: #999;}
nav ul li a{color: #fff; padding: 0 30px; line-height:25px; font-size:11px; font-family:arial; display: block; text-decoration:none;}
nav ul li a:hover{background-color: #999;}
nav ul li ul li{float: none; position: relative;}
nav ul li ul{position: absolute; top:25px; left:0; display: none; background-color: #000; width:150px;}
nav ul li:hover > ul{display: block;}
nav ul li ul li a{white-space: nowrap; line-height:25px;}
nav ul li ul li ul{position: absolute; top:0; left:145px; display: none; background-color: #000; width:150px;}*/
</style>
    </head>
    <body>
        

     <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="collapse navbar-collapse">
        	<?=buildMenu(0,$menu)?>

         <!--  <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul> -->
        </div><!--/.nav-collapse -->
      </div>
    </div>
		


     <div class="container">

      <h1>Bootstrap starter template</h1>
      <p>Use this document as a way to quick start any new project.<br> All you get is this message and a barebones HTML document.</p>

    </div> <!-- /container -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/2.1.0/placeholders.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.1/select2.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.min.js"></script>
    



    <script>
	$(document).ready(function() {
		 //$('#element').validate();
		 $('select').select2();
		 // $('.dropdown-toggle').dropdown();
	});
</script>

<script type="text/javascript">

/* This is for the confirm modal for deleting leads */
$(document).ready(function() {
	$('a[data-confirm]').click(function(ev) {
    console.log('data-confirm');
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});


});


</script>



    </body>
    </html>


				


<?
//SQL
/*
  CREATE TABLE menu (
	id int(11) NOT NULL auto_increment,
	label varchar(50) NOT NULL default '',
	link varchar(100) NOT NULL default '#',
	parent int(11) NOT NULL default '0',
	sort int(11) default NULL,
	PRIMARY KEY (id)
  )

INSERT INTO menu (id, label, link, parent, sort) VALUES (1, 'Home', '#home', 0, 0);
INSERT INTO menu (id, label, link, parent, sort) VALUES (2, 'Code', '#code', 0, 1);
INSERT INTO menu (id, label, link, parent, sort) VALUES (3, 'Contact', '#contact', 0, 3);
INSERT INTO menu (id, label, link, parent, sort) VALUES (4, 'PHP', '#php', 2, NULL);
INSERT INTO menu (id, label, link, parent, sort) VALUES (5, 'CSS', '#css', 2, NULL);
INSERT INTO menu (id, label, link, parent, sort) VALUES (6, 'Scripts', '#scripts', 4, 1);
INSERT INTO menu (id, label, link, parent, sort) VALUES (7, 'Help', '#help', 4, 0);
INSERT INTO menu (id, label, link, parent, sort) VALUES (8, 'Archive', '#archive', 6, NULL);
INSERT INTO menu (id, label, link, parent, sort) VALUES (9, 'Snippets', '#snippets', 8, NULL);
 */



//Menu Builder
function buildMenu($parent,$menu){
	static $ulCount = 0;
	$html = "";
	if (isset($menu['parents'][$parent])){
		if($ulCount == 0){
			$html .= "<ul class='nav navbar-nav'>\n";
		}else{
			$html .= "<ul class='dropdown-menu'>\n";
		}
		

		foreach ($menu['parents'][$parent] as $itemid ) {
			if(!isset($menu['parents']{$itemid})){
				$html .= "<li>\n \t<a href='" . $menu['items'][$itemid]['link'] ."'>" . $menu['items'][$itemid]['label'] . "</a>\n</li> \n";
			}
			if (isset($menu['parents'][$itemid])){
				$ulCount++;
				$html .= "\t<li class='dropdown'>\n \t<a href='" .$menu['items'][$itemid]['link'] ."' class='dropdown-toggle' data-toggle='dropdown'>" . $menu['items'][$itemid]['label'] . "<b class='caret'></b></a> \n";
				$html .= buildMenu($itemid,$menu);
				$html .= "\t\t</li> \n";
			}

		}
		$html .= "\t</ul>\n";
		
	}
	return $html;
}
?>

<!-- <ul>
   <li>
      <a href='#code'>Code</a> 
      <ul>
         <li>
            <a href='#css'>CSS</a>
         </li>
         <li>
            <a href='#php'>PHP</a> 
            <ul>
               <li>
                  <a href='#help'>Help</a>
               </li>
               <li>
                  <a href='#scripts'>Scripts</a> 
                  <ul>
                     <li>
                        <a href='#archive'>Archive</a> 
                        <ul>
                           <li>
                              <a href='#snippets'>Snippets</a>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </li>
            </ul>
         </li>
      </ul>
   </li>
   <li>
      <a href='#contact'>Contact</a>
   </li>
   <li>
      <a href='#home'>Home</a>
   </li>
</ul> -->