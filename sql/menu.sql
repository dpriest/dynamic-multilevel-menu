/*
Multi Level Menu
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(100) NOT NULL DEFAULT '#',
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Home', '#home', '0', '0');
INSERT INTO `menu` VALUES ('2', 'Code', '#code', '0', '1');
INSERT INTO `menu` VALUES ('3', 'Contact', '#contact', '0', '3');
INSERT INTO `menu` VALUES ('4', 'PHP', '#php', '2', null);
INSERT INTO `menu` VALUES ('5', 'CSS', '#css', '2', null);
INSERT INTO `menu` VALUES ('6', 'Scripts', '#scripts', '4', '1');
INSERT INTO `menu` VALUES ('7', 'Help', '#help', '4', '0');
INSERT INTO `menu` VALUES ('8', 'Archive', '#archive', '6', null);
INSERT INTO `menu` VALUES ('9', 'Snippets', '#snippets', '8', null);
